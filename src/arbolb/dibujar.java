package arbolb;

import arbolb.nodo;
import arbolb.arbolB;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author AndresFWilT
 */

public class dibujar {

    arbolB ParbB;
    nodo nodo;

    public void pintar(Graphics g, nodo padre, nodo hijo) {

    }

    public void DibujaArbol(Graphics g, int x, int y, nodo nodo) {
        DibujaArbol2(g, 500, y, nodo);
    }

    public void DibujaArbol2(Graphics g, int x, int y, nodo nodo) {
        g.setFont(new java.awt.Font("Nirmala UI", 1, 14));
        nodo padre = nodo;
        if (padre == null) {
        } else {
            int extra;
            switch (padre.getNivel()) {
                case 0:
                    extra = 0;
                    break;
                case 1:
                    extra = 120;
                    break;
                case 2:
                    extra = 180;
                    break;
                case 3:
                    extra = 220;
                    break;
                case 4:
                    extra = 350;
                    break;
                default:
                    extra = 0;
            }
            if (nodo.getValoresNodos().size() > 1) {
                String nodos = "";
                for (int i = 0; i <= nodo.getValoresNodos().size() - 1; i++) {
                    nodos = nodos + nodo.getValoresNodos().get(i);
                    if (!nodo.getValoresNodos().get(i).equals(nodo.getValoresNodos().get(nodo.getValoresNodos().size() - 1))) {
                        nodos = nodos + ",";
                    }
                }
                nodos = nodos + "";
                g.setColor(Color.black);
                g.drawString(nodos, x, y);
                g.setFont(new java.awt.Font("Nirmala UI", 1, 16));
             } else {
                String nodos = "" + nodo.getValoresNodos().get(0) + " ";
                g.setColor(Color.black);
                g.drawString(nodos, x, y);
                g.setFont(new java.awt.Font("Nirmala UI", 1, 18));
            }
            if (padre.hijoizquierda != null && padre.hijoizquierda.getValoresNodos() != null) {
                g.setColor(Color.red);
                g.drawLine(x - 3, y + 3, (x - 209 + extra), y + 25);
            }
            if (padre.hijoderecha != null && padre.hijoderecha.getValoresNodos() != null) {
                g.setColor(Color.red);
                g.drawLine(x + 6, y + 3, (x + 216 - extra), y + 25);
            }
            if (padre.hijomitad != null && padre.hijoderecha.getValoresNodos() != null) {
                g.setColor(Color.red);
                g.drawLine(x, y + 3, x + 3, y + 20);
            }
            DibujaArbol2(g, (x - 220 + extra), y + 30, padre.hijoizquierda);
            DibujaArbol2(g, (x + 220 - extra), y + 30, padre.hijoderecha);
            DibujaArbol2(g, x, y + 30, padre.hijomitad);
        }
    }

    public void DibujarFlechas(Graphics g, int x, int y, int difx, int dify) {
        g.drawLine(x, y, x + difx, y + dify);

    }

    public static void inicializaArbol(Graphics g, int x, int y, String i) {
        g.drawString(i, x, y);
    }

}
