package arbolb;

/**
 *
 * @author AndresFWilT
 */

public class subNodo {

    private String valor;
    private nodo hi;
    private nodo hd;

    public subNodo(String val, nodo hi, nodo hd) {
        setValor(val);
        this.setHi(hi);
        this.setHd(hd);
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public nodo getHi() {
        return hi;
    }

    public void setHi(nodo hi) {
        this.hi = hi;
    }

    public nodo getHd() {
        return hd;
    }

    public void setHd(nodo hd) {
        this.hd = hd;
    }

    public nodo toRaiz() {
        nodo nodo = new nodo(getHd().getOrden(), this);
        return nodo;
    }
}
