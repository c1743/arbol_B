package arbolb;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author AndresFWilT
 */

public class ventana extends javax.swing.JFrame {

    arbolB arbol;
    public ArrayList<String> array = new ArrayList<>();

    public ventana() {
        this.setTitle("arbol B");
        initComponents();
        arbol = new arbolB(1);
        bLimpiar.setVisible(false);
        lArbol.setVisible(false);
        this.setResizable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tInsertar = new javax.swing.JTextField();
        bInsertar = new javax.swing.JButton();
        tRetirar = new javax.swing.JTextField();
        bRetirar = new javax.swing.JButton();
        bLimpiar = new javax.swing.JButton();
        bGraficar = new javax.swing.JButton();
        pGraficos = new javax.swing.JPanel();
        lArbol = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));

        bInsertar.setBackground(new java.awt.Color(0, 0, 0));
        bInsertar.setForeground(new java.awt.Color(255, 255, 255));
        bInsertar.setText("insertar");
        bInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bInsertarActionPerformed(evt);
            }
        });

        bRetirar.setBackground(new java.awt.Color(0, 0, 0));
        bRetirar.setForeground(new java.awt.Color(255, 255, 255));
        bRetirar.setText("borrar");
        bRetirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bRetirarActionPerformed(evt);
            }
        });

        bLimpiar.setText("limpiar");
        bLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bLimpiarActionPerformed(evt);
            }
        });

        bGraficar.setBackground(new java.awt.Color(0, 0, 0));
        bGraficar.setForeground(new java.awt.Color(255, 255, 255));
        bGraficar.setText("graficar");
        bGraficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bGraficarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pGraficosLayout = new javax.swing.GroupLayout(pGraficos);
        pGraficos.setLayout(pGraficosLayout);
        pGraficosLayout.setHorizontalGroup(
            pGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1157, Short.MAX_VALUE)
        );
        pGraficosLayout.setVerticalGroup(
            pGraficosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 441, Short.MAX_VALUE)
        );

        lArbol.setFont(new java.awt.Font("Nirmala UI", 1, 24)); // NOI18N
        lArbol.setText("Arbol");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(bLimpiar)
                        .addGap(51, 51, 51))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 69, Short.MAX_VALUE)
                                .addComponent(pGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tInsertar, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bInsertar)
                                .addGap(18, 18, 18)
                                .addComponent(tRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bRetirar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bGraficar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lArbol, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(578, 578, 578))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tInsertar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bInsertar)
                        .addComponent(tRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bRetirar)
                        .addComponent(bGraficar))
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bLimpiar)
                .addGap(27, 27, 27)
                .addComponent(lArbol)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(pGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bInsertarActionPerformed
        insertar(arbol);
        bLimpiarActionPerformed(evt);
        lArbol.setVisible(false);
    }//GEN-LAST:event_bInsertarActionPerformed

    private void bRetirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bRetirarActionPerformed
        eliminar(arbol);
        lArbol.setVisible(false);
        bLimpiarActionPerformed(evt);
    }//GEN-LAST:event_bRetirarActionPerformed

    private void bGraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bGraficarActionPerformed
        lArbol.setVisible(true);
        graficar(arbol);
    }//GEN-LAST:event_bGraficarActionPerformed

    private void bLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bLimpiarActionPerformed
        pGraficos.repaint();
    }//GEN-LAST:event_bLimpiarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bGraficar;
    private javax.swing.JButton bInsertar;
    private javax.swing.JButton bLimpiar;
    private javax.swing.JButton bRetirar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lArbol;
    private javax.swing.JPanel pGraficos;
    private javax.swing.JTextField tInsertar;
    private javax.swing.JTextField tRetirar;
    // End of variables declaration//GEN-END:variables

    private void insertar(arbolB pB) {
        String strValor = tInsertar.getText().trim();
        if (pB.buscar(strValor) == null) {
            pB.Insertar(strValor);
            array.add(strValor);
        } else {
            JOptionPane.showMessageDialog(null, "El valor esta repetido");
        }
        tInsertar.setText("");
        tInsertar.requestFocus();
    }

    private void eliminar(arbolB pB) {
        String strValor = tRetirar.getText().trim();
        pB.eliminar(strValor);
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).compareTo(strValor) == 0) {
                array.remove(i);
            }
        }
        tRetirar.setText("");
        tRetirar.requestFocus();
    }

    private void graficar(arbolB pB) {
        arbol.imprimirarbol(pGraficos.getGraphics(), arbol);
    }
}
